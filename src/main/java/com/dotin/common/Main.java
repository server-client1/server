package com.dotin.common;

import com.dotin.entity.Deposit;
import com.dotin.entity.Server;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;


public class Main {
    public static ArrayList<ClientHandler> connections = new ArrayList<ClientHandler>();
    private final static Logger logger = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) throws IOException {
        Server server = readJson();
       List<Deposit> list=server.getDeposits();
        for (Deposit deposit: list ) {
            System.out.println(deposit.getDepositId());

        }
        FileHandler fileHandler = new FileHandler(server.getLogFileName() + ".log");
        SimpleFormatter simpleFormatter = new SimpleFormatter();
        fileHandler.setFormatter(simpleFormatter);
        logger.addHandler(fileHandler);
        logger.setUseParentHandlers(false);
        ServerSocket serverSocket = new ServerSocket(Integer.parseInt(server.getServerPortNumber()));
        logger.info("Server is Running");
        // running infinite loop for getting
        // client request
        while (true) {
            Socket socket = null;

            try {
                // socket object to receive incoming client requests
                socket = serverSocket.accept();
                logger.info("A new client is connected : " + socket);

                System.out.println("A new client is connected : " + socket);

                // obtaining input and out streams
                DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
                DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());

                System.out.println("Assigning new thread for this client");
                logger.info("Assigning new thread for this client");
                logger.info("create a new thread object");
                Thread clientThread = new ClientHandler(socket, dataInputStream, dataOutputStream, server, logger);

                logger.info("Invoking the start() method");
                clientThread.start();

            } catch (Exception e) {
                socket.close();
                e.printStackTrace();
                logger.throwing("Main Class", "main", e);
            }
        }
    }


    private static Server readJson() throws IOException {
        String filename = "Server.json";
        Gson gson = new Gson();
        JsonReader reader = new JsonReader(new FileReader(filename));
        Server server = gson.fromJson(reader, Server.class);
        return server;
    }
}
