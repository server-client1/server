package com.dotin.entity;

import java.util.concurrent.locks.Lock;

public class Deposit {
    private String customerName;
    private String depositId;
    private String initialBalance;
    private String upperBound;


    public Deposit(String customerName, String depositId, String initialBalance, String upperBound) {
        this.customerName = customerName;
        this.depositId = depositId;
        this.initialBalance = initialBalance;
        this.upperBound = upperBound;
    }



    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getDepositId() {
        return depositId;
    }

    public void setDepositId(String depositId) {
        this.depositId = depositId;
    }

    public String getInitialBalance() {
        return initialBalance;
    }

    public void setInitialBalance(String initialBalance) {
        this.initialBalance = initialBalance;
    }

    public String getUpperBound() {
        return upperBound;
    }

    public void setUpperBound(String upperBound) {
        this.upperBound = upperBound;
    }



    public Integer deposit(Integer amount) {
        synchronized (this) {
            Integer lastBalance = Integer.parseInt(this.getInitialBalance()) + amount;
            return lastBalance;
        }

    }

    public Integer withdraw(Integer amount) {

        synchronized (this) {
            Integer lastBalance = Integer.parseInt(this.getInitialBalance()) - amount;
            return lastBalance;
        }
    }

}
