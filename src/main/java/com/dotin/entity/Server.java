package com.dotin.entity;

import java.util.List;

public class Server {
    private String serverPortNumber;
    private List<Deposit> deposits;
    private String logFileName;

    public String getServerPortNumber() {
        return serverPortNumber;
    }

    public void setServerPortNumber(String serverPortNumber) {
        this.serverPortNumber = serverPortNumber;
    }

    public void setDeposits(List<Deposit> deposits) {
        this.deposits = deposits;
    }

    public String getLogFileName() {
        return logFileName;
    }

    public void setLogFileName(String logFileName) {
        this.logFileName = logFileName;
    }


    public List<Deposit> getDeposits() {
        return deposits;
    }

    public Server(String serverPort, List<Deposit> deposits, String logFileName) {
        this.serverPortNumber = serverPort;
        this.deposits = deposits;
        this.logFileName = logFileName;
    }


}
