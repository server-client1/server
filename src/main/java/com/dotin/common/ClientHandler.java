package com.dotin.common;

import com.dotin.entity.*;

import java.io.*;
import java.net.Socket;

import java.util.ArrayList;

import java.util.List;
import java.util.logging.Logger;

public class ClientHandler extends Thread {
    final DataInputStream dataInputStream;
    final DataOutputStream dataOutputStream;
    final Socket socket ;
    final Server server;
    final Logger logger;


    // Constructor
    public ClientHandler(Socket socket, DataInputStream dataInputStream, DataOutputStream dataOutputStream1, Server server, Logger logger) {
        this.socket = socket;
        this.dataInputStream = dataInputStream;
        this.dataOutputStream= dataOutputStream1;
        this.server = server;
        this.logger = logger;

    }

    @Override
    public void run() {

        try {
            List<Response> responseList = new ArrayList<>();
            ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
            // Ask user what he wants
            List<Transaction> transactionList = (List<Transaction>) objectInputStream.readObject();
            logger.info("Receive Transaction from" + socket);
            for (int i = 0; i < transactionList.size(); i++) {
                Transaction transaction = transactionList.get(i);
                Response response = validate(transaction.getTransactionType(), transaction.getDepositId(), transaction.getAmount());
                responseList.add(response);


            }
            objectOutputStream.writeObject(responseList);
            logger.info("Send Response for " + socket);
            objectOutputStream.reset();


        } catch (IOException e) {
            e.printStackTrace();
            logger.throwing("ClientHandler Class", "Rum", e);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            logger.throwing("ClientHandler Class", "Rum", e);
        }

    }





    private Response validate(TransactionType transactionType, String depositId, String amount) {
        logger.exiting("ClientHandler Class", "validate Transaction", "check Transaction");
        List<Deposit> depositList = server.getDeposits();
        for (Deposit deposit : depositList) {
            if (deposit.getDepositId().equals(depositId)) {
                synchronized (deposit) {
                    logger.info("The Client " + socket + "is Running");
                    switch (transactionType) {
                        case WITHDRAW:
                            if (Integer.parseInt(deposit.getInitialBalance()) < Integer.parseInt(amount)) {
                                logger.info("Balance is not enough for " + depositId);
                                Response response = new Response(transactionType.name().toString(), "fail:Balance is not enough", depositId, deposit.getInitialBalance());
                                return response;
                            } else {

                                Integer finalBalance = deposit.withdraw(Integer.parseInt(amount));
                                deposit.setInitialBalance(String.valueOf(finalBalance));
                                logger.info("withdraw is commit for " + depositId + " from " + socket);
                                Response response = new Response(transactionType.name().toString(), "commit", depositId, String.valueOf(finalBalance));
                                return response;

                            }


                        case DEPOSIT:
                            if (Integer.parseInt(amount) + Integer.parseInt(deposit.getInitialBalance()) > Integer.parseInt(deposit.getUpperBound())) {
                                logger.info(" UpperBalance is full for " + depositId + " from " + socket);
                                Response response = new Response(transactionType.name().toString(), "fail:UpperBalance is not valid", depositId, deposit.getInitialBalance());

                            } else {
                                Integer finalBalance = deposit.deposit(Integer.parseInt(amount));
                                deposit.setInitialBalance(String.valueOf(finalBalance));
                                logger.info("deposit is commit for " + depositId + " from " + socket);
                                Response response = new Response(transactionType.name().toString(), "commit", depositId, String.valueOf(finalBalance));
                                return response;
                            }


                        default:
                            logger.info("Transaction not valid for " + depositId + " from " + socket);
                            Response response = new Response(transactionType.name().toString(), "fail:Transaction not valid", depositId, deposit.getInitialBalance());
                            return response;

                    }

                }
            }
        }
        logger.info("depositId not valid " + socket);
        Response response = new Response(transactionType.name().toString(), "fail:DepositId not valid", depositId);
        return response;


    }
}
