package com.dotin.entity;

import java.io.Serializable;

public class Response implements Serializable {
    private static final long serialVersionUID = 6529685098267757690L;
    private String depositId;
    private String finalBalance;
    private String transactionType;
    private String transactionResult;

    public Response(String transactionType, String result, String depositId, String finalBalance) {
        this.transactionType = transactionType;
        this.transactionResult = result;
        this.depositId = depositId;
        this.finalBalance = finalBalance;
    }

    public Response(String transactionType, String result, String depositId) {
        this.transactionType = transactionType;
        this.transactionResult = result;
        this.depositId = depositId;
    }


    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getDepositId() {
        return depositId;
    }

    public void setDepositId(String depositId) {
        this.depositId = depositId;
    }

    public String getTransactionResult() {
        return transactionResult;
    }

    public void setTransactionResult(String transactionResult) {
        this.transactionResult = transactionResult;
    }

    public String getFinalBalance() {
        return finalBalance;
    }

    public void setFinalBalance(String finalBalance) {
        this.finalBalance = finalBalance;
    }


}
