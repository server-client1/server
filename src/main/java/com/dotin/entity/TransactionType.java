package com.dotin.entity;

public enum TransactionType {
    DEPOSIT(0),
    WITHDRAW(1);
    private int value;

    TransactionType(int value) {

        this.value = value;
    }

    public int getValue() {

        return value;
    }

    public static TransactionType getTransactionType(int value) {
        for (TransactionType transactionType : TransactionType.values()) {
            if (transactionType.getValue() == value)
                return transactionType;

        }
        return null;

    }
}
